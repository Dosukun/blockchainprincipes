/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from "react";
import { BiCodeBlock } from "react-icons/bi";
import { useLocation } from "react-router-dom";
import LoginForm from "../Components/LoginForm";
import SignInForm from "../Components/SignInForm";
import "./Views.css";

function LoginPage(props) {
  let location = useLocation();
  const [asAccount, setAccount] = useState(false);

  useEffect(() => {
    if (typeof location.props !== "undefined") {
      setAccount(location.props.asAccount);
      console.log(location.props.asAccount);
    }
  }, [location.props]);

  return (
    <div className="Main">
      <div className="PresDiv">
        <div className="BrandIcon">
          <BiCodeBlock />
        </div>
        <h className="BrandTypo">BlockChain Simulator</h>
      </div>
      <div className="FlexRawCenter">
        <div className="FlexColCenter">
          {asAccount && (
            <>
              <LoginForm />
              <button className="LoginButton" onClick={() => setAccount(false)}>
                Vous n'avez pas de compte? Inscrivez-vous!
              </button>
            </>
          )}
          {!asAccount && (
            <>
              <SignInForm setAccount={setAccount} />
              <button
                className="LoginButton"
                style={{ width: "70%" }}
                onClick={() => setAccount(true)}
              >
                Vous avez deja un compte? Connectez vous!
              </button>
              <button
                style={{ width: "70%" }}
                className="LoginButton"
                onClick={() => setAccount(true)}
              >
                Continuez avec Github
              </button>
              <button
                className="LoginButton"
                style={{ width: "70%" }}
                onClick={() => setAccount(true)}
              >
                Continuez avec Microsoft
              </button>
            </>
          )}
        </div>
        <img
          src="https://d2k1ftgv7pobq7.cloudfront.net/meta/p/res/images/308998dcb3ed5ab3d01217a4d24ffa03/hero-a.svg"
          alt=""
        />
      </div>
    </div>
  );
}

export default LoginPage;
