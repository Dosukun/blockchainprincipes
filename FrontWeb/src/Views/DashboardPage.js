/* eslint-disable react/jsx-filename-extension */
import React, { useState, useEffect } from "react";
import { BiCodeBlock } from "react-icons/bi";
import Axios from "axios";
import Wallet from "../Components/Wallet";
import Miner from "../Components/Miner";
import Market from "../Components/Market";
import Node from "../Components/NodeTrans";
import "./Views.css";
import "bootstrap/dist/css/bootstrap.min.css";

function DashboardPage() {
  
  return (
    <div className="Main2">
      <div className="PresDiv">
        <div className="BrandIcon">
          <BiCodeBlock />
        </div>
        <h1 className="BrandTypo">BlockChain Simulator</h1>
      </div>
      <div className="Position">
        <Wallet />
        <Node />
        <Miner />
        <Market />
      </div>
    </div>
  );
}

export default DashboardPage;
