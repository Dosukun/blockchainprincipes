import React from "react";
import NavBar from "../Components/NavBar";
import StarterForm from "../Components/StarterForm";
import "./Views.css";

function LandingPage() {
  return (
    <div className="Main">
      <NavBar />
      <StarterForm />
    </div>
  );
}


export default LandingPage;
