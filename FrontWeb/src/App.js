import React from "react";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import PrivateRoute from "./Utils/PrivateRoutes";
import LandingPage from "./Views/LandingPage";
import LoginPage from "./Views/LoginPage";
import DashboardPage from "./Views/DashboardPage.js";
import './App.css';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          {/* <PrivateRoute exact component={Dashboard} path="/dashboard" /> */}
          <Route exact component={LandingPage} path="/" />
          <Route path="/login" render={(props) => <LoginPage {...props} />} />
          <PrivateRoute
            path="/dashboard"
            component={(props) => <DashboardPage {...props} />}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}


export default App;
