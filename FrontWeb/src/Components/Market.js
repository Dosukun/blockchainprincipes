import Axios from "axios";
import React, { useState, useEffect } from "react";
import { Card, Button, InputGroup, Table, FormControl } from "react-bootstrap";

import "./Components.css";

function Market() {
    const [pubK, setPubk] = useState("");
    const [goods, setGoods] = useState("");
    const [price, setPrice] = useState("");
    const [allGoods, setAllGoods] = useState();

    const addItems = () => {
        console.log(pubK, goods, price)
        Axios("http://localhost:3001/addItems", {
      method: "POST",
      data: {
        pub: pubK,
        goods: goods,
        price: price
      },
    })
      .then(() => {
        Axios("http://localhost:3001/getItems", {
            method: "GET"
          })
            .then((response) => {
              setAllGoods(response.data);
            })
            .catch((err) => {
              console.log(err);
            });
      })
      .catch((err) => {
        console.log(err);
      });
    }

    const getMarket = () => {
        Axios("http://localhost:3001/getItems", {
            method: "GET"
        })
            .then((response) => {
                setAllGoods(response.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    useEffect(() => {
            getMarket();
            }, []);

  return (
    <div className="Partdiv">
      <Card>
        <Card.Header style={{ background: "#9e79d9" }}>
          <div className="Sypo">Market Place</div>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            <div className="PresDiv">
              <div className="LeftPresDiv">
                <Button style={{ margin: "5px", width: "300px" }}>Goods/Services</Button>
              </div>
              <div div className="RightPresDiv">
                <div style={{display: "flex",  flexDirection: "row", width: "1200px"}}>
                <InputGroup style={{ margin: "5px" }} size="l" className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-sm">Public Key</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl aria-label="" aria-describedby="inputGroup-sizing-m" value={pubK} onChange={(e) => {setPubk(e.target.value)}}/>
                    </InputGroup>
                    <InputGroup style={{ margin: "5px" }} size="l" className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-sm">Goods/Services</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl aria-label="" aria-describedby="inputGroup-sizing-m" value={goods} onChange={(e) => {setGoods(e.target.value)}}/>
                    </InputGroup>
                    <InputGroup style={{ margin: "5px" }} size="l" className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-sm">Price</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl aria-label="" aria-describedby="inputGroup-sizing-m" value={price} onChange={(e) => {setPrice(e.target.value)}}/>
                    </InputGroup>
                <Button onClick={()=> addItems()}>Save!</Button>
                </div>
                <div style={{ width: "1200px" }}>
                <Table responsive striped bordered hover >
                    <thead >
                        <tr>
                            <th>Public Key</th>
                            <th>Product/Service offered</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {allGoods != null && Object.keys(allGoods).map(function (key) {
                        return (
                            <tr>
                                <td>{allGoods[key].publicKey}</td>
                                <td>{allGoods[key].good}</td>
                                <td>{allGoods[key].amount}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
                    <Button onClick={getMarket}>Update table</Button>
                </div>
              </div>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Market;
