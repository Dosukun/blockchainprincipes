import React from "react";
import "./Components.css";

function StarterForm() {
  return (
    <div>
      <div className="PresDiv">
        <div className="LeftPresDiv">
          <h className="LTypo">Avec BlockChain Simulator</h>
          <h className="LTypo">Vous pouvez simuler</h>
          <h className="LTypo">le comportement</h>
          <h className="LTypo">d'une blockchain</h>
          <h className="LTypo">afin de mieux la comprendre!</h>
          <h className="MTypo">Utilisez votre Wallet</h>
          <h className="MTypo">Connectez vous au reseau de nodes</h>
          <h className="MTypo">Validez et hashez les blocks</h>
          <h className="MTypo">Minez!</h>
          <h className="MTypo">Et tout cela de maniere simple et éducative!</h>
        </div>
        <div className="RightPresDiv">
          <img
            src="https://d2k1ftgv7pobq7.cloudfront.net/meta/p/res/images/308998dcb3ed5ab3d01217a4d24ffa03/hero-a.svg"
            alt=""
          />
        </div>
      </div>
      <div className="BottomLogin">
        <form>
          <input
            className="EmailForm"
            type="text"
            // onChange={this.myChangeHandler}
            placeholder="E-mail"
          />
        </form>
        <button className="ButtonBottom">
          <h className="MTypo">Inscrivez-vous, c'est gratuit !</h>
        </button>
      </div>
    </div>
  );
}

export default StarterForm;
