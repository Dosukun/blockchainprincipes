import React, { useState, useEffect } from "react";
import Axios from "axios";
import { Container, Row, Col, Card, Button, InputGroup, FormControl, Form} from "react-bootstrap";
import "./Components.css";

function Wallet() {
  const [user, setUser] = useState({});
  const [mode, setMode] = useState(1);
  const [signature, setSignature] = useState("Sign your transaction");
  const [amount, setAmount] = useState(undefined);
  const [fee, setFee] = useState(undefined);
  const [from, setFrom] = useState(undefined);
  const [to, setTo] = useState(undefined);
  const [privateKeyForSign, setPrivateKeyForSign] = useState(undefined);
  const [isSign, setIsSign] = useState(false);

  useEffect(() => {
    Axios("http://localhost:3001/get", {
      method: "POST",
      data: {
        uid: localStorage.getItem("uid")
      },
    })
      .then((response) => {
        setUser(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onRandomClick = () => {
    Axios("http://localhost:3001/regenkey", {
      method: "PUT",
      data: {
        uid: localStorage.getItem("uid")
      },
    })
    .then((response) => {
      setUser({...user, privateKey: response.data.privateKey, publicKey: response.data.publicKey});
    })
    .catch((err) => {
      console.log(err);
    });
  };

  const onSignClick = async () => {
    const data={
      amount,
      fee,
      from,
      to
    }
    Axios("http://localhost:3001/signKey", {
      method: "POST",
      data: {
        privateKey: privateKeyForSign,
        data: JSON.stringify(data)
      },
    })
    .then((response) => {
      console.log("pomme",response);
      setSignature(response.data.signature)
    })
    .catch((err) => {
      console.log(err);
    });
    setIsSign(true);
  };

  const onPublishClick = async () => {
    Axios("http://localhost:3001/addGossip", {
      method: "POST",
      data: {
        amount,
        fee,
        from,
        to,
        signature
      },
    })
    .then((response) => {
      console.log("pomme",response);
      setSignature(response.data.signature)
    })
    .catch((err) => {
      console.log(err);
    });
    setSignature("Sign your transaction");
  }

  return (
    <div className="Partdiv">
      <Card>
        <Card.Header style={{ background: "#79d98e" }}>
          <div className="Sypo">My local Wallet software</div>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            <div className="PresDiv">
              <div className="LeftPresDiv">
                <Container fluid>
                  <Row> 
                    <Form>
                      <Form.Group controlId="userBalance">
                          <Form.Label style = {{marginLeft: "5px", marginRight: "5px"}}>User : {user.email}</Form.Label>
                          <InputGroup style = {{marginLeft: "5px", marginRight: "5px"}}>
                            <InputGroup.Prepend>
                              <InputGroup.Text>Balance</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control type="Text" disabled value={user.balance}/>
                          </InputGroup>
                        </Form.Group>
                      </Form>
                  </Row>
                  <Row>
                    <Button style={{ margin: "5px", width: "300px" }} onClick={() => { setMode(1) }}>My BlockChain Keys</Button>
                  </Row>
                  <Row>
                    <Button style={{ margin: "5px", width: "300px" }} onClick={() => { setMode(2) }}>Transaction Initiation</Button>
                  </Row>
                </Container>
                </div>
                <div div className="RightPresDiv">
                  {mode === 1 && (
                    <>
                      <Form>
                        <Form.Group>
                          <Form.Label>Public / Private Key Pairs</Form.Label>
                          <InputGroup>
                            <InputGroup.Prepend>
                              <InputGroup.Text id="inputGroup-sizing-sm">Public Key</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl value={user.publicKey} disabled/>
                          </InputGroup>
                        </Form.Group>
                        <Form.Group>
                          <InputGroup>
                            <InputGroup.Prepend>
                              <InputGroup.Text id="inputGroup-sizing-sm">Private Key</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl value={user.privateKey} disabled/>
                          </InputGroup>
                        </Form.Group>
                        <Button variant="primary" onClick={onRandomClick}>
                          Random
                        </Button>
                      </Form>
                    </>)
                  }
                  {mode === 2 && (
                  <>
                    <Form>
                      <Form.Group controlId="formAmount">
                        <Form.Label>Transaction</Form.Label>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text>Amount</InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control type="number" step={0.01} min={0.01} value={amount} onChange={(e)=>setAmount(e.target.value)}/>
                        </InputGroup>
                      </Form.Group>
                      <Form.Group controlId="formFee">
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text>Fee</InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control type="number" step={0.01} min={0.01} value={fee} onChange={(e)=>setFee(e.target.value)}/>
                        </InputGroup>
                      </Form.Group>
                      <Form.Group controlId="formSource">
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text>From:</InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control type="text" value={from} onChange={(e)=>setFrom(e.target.value)}/>
                        </InputGroup>
                      </Form.Group>
                      <Form.Group controlId="formDestination">
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text>To:</InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control type="text" value={to} onChange={(e)=>setTo(e.target.value)}/>
                        </InputGroup>
                      </Form.Group>
                      <Form.Group controlId="formPrivateKey">
                        <Form.Label>Private Key</Form.Label>
                        <Form.Control type="Text" value={privateKeyForSign} onChange={(e)=>setPrivateKeyForSign(e.target.value)}/>
                      </Form.Group>
                      <Button variant="primary" onClick={onSignClick}>
                        Sign
                      </Button>
                      <Form.Group controlId="formPrivateKey">
                        <Form.Label>Message Signature</Form.Label>
                        <Form.Control type="Text" disabled value={signature}/>
                      </Form.Group>
                      <Button disabled={!isSign} variant="primary" onClick={() => {onPublishClick();setIsSign(false)}}>
                        Publish transaction to the network
                      </Button>
                    </Form>
                    </>)
                  }
                </div>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Wallet;
