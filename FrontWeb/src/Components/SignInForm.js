import React, { useState } from "react";
import Axios from "axios";
import { login, saveuid } from "../Utils/utils";
import "./Components.css";

function SignInForm({setAccount}) {
  const [userName, setUser] = useState("User");
  const [firstName, setFirst] = useState("Firstname");
  const [lastName, setLast] = useState("Lastname");
  const [email, setEmail] = useState("Email");
  const [Password, setPass] = useState("Password");

  const handleForm = () => {
    Axios("http://localhost:3001/register", {
      method: "POST",
      data: {
        firstname: firstName,
        lastname: lastName,
        email: email,
        username: userName,
        password: Password,
      },
    })
      .then((response) => {
        console.log(response);
        login(response.data.token);
        saveuid(response.data.uid);
        setAccount(true);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="LoginForm">
      <p>Inscrivez-vous dés maintenant!</p>
      <div className="FormBlock">
        <label>
          Firstname:
          <input
            onChange={(e) => setFirst(e.target.value)}
            className="InputForm"
            type="text"
            placeholder="FirstName"
            name="UserName"
          />
        </label>
        <label>
          Lastname:
          <input
            onChange={(e) => setLast(e.target.value)}
            className="InputForm"
            type="text"
            placeholder="Lastname"
            name="UserName"
          />
        </label>
        <label>
          UserName:
          <input
            onChange={(e) => setUser(e.target.value)}
            className="InputForm"
            type="text"
            placeholder="UserName"
            name="UserName"
          />
        </label>
        <label>
          Email:
          <input
            onChange={(e) => setEmail(e.target.value)}
            className="InputForm"
            type="email"
            placeholder="Email"
            name="Email"
          />
        </label>
        <label>
          Password:
          <input
            onChange={(e) => setPass(e.target.value)}
            className="InputForm"
            type="text"
            placeholder="PassWord"
            name="PassWord"
          />
        </label>
      </div>
      <button className="LoginButton" onClick={() => handleForm()}>
        C'est parti!
      </button>
    </div>
  );
}

export default SignInForm;
