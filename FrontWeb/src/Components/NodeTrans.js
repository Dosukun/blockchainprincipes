import React, { useState } from "react";
import { Card, Button} from "react-bootstrap";
import "./Components.css";
import NetworkNode from "./networkNode";
import PersonalBlock from "./personalBlock";
import Gossip from "./Gossip";
import Mempool from "./Mempool"

function NodeTrans() {
  const [mode, setMode] = useState(0);
  return (
    <div className="Partdiv">
      <Card>
        <Card.Header style={{ background: "#9bdec9" }}>
          <div className="Sypo">My local Node Software</div>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            <div className="PresDiv">
              <div className="LeftPresDiv">
                <Button style={{ margin: "5px", width: "300px" }} onClick={() => setMode(0)}>Network</Button>
                <Button style={{ margin: "5px", width: "300px" }} onClick={() => setMode(1)}>
                  My Personal BlockChain
                </Button>
                <Button style={{ margin: "5px", width: "300px" }} onClick={() => setMode(2)}>Gossip</Button>
                <Button style={{ margin: "5px", width: "300px" }} onClick={() => setMode(3)}>
                  Mempool: Transaction to be mined
                </Button>
              </div>
              <div className="RightPresDiv" style={{width: 1500}}>
                <div style={{display: mode === 0 ? "block" : "none"}}>
                  <NetworkNode/>
                </div>
                {mode === 1 && <PersonalBlock/>}
                {mode === 2 && <Gossip/>}
                {mode === 3 && <Mempool/>}
              </div>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

export default NodeTrans;
