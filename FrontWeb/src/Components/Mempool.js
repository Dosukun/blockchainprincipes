import React, { useEffect, useState } from "react";
import { Card, Button, Table } from "react-bootstrap";
import "./Components.css";
import Axios from "axios";

function Mempool(props) {
  const [mempool, setMempool] = useState({});

  const handleForm = () => {
    Axios("http://localhost:3001/getMempool", {
      method: "POST",
      data: {
        uid: localStorage.getItem("uid"),
      }
    })
      .then((response) => {
        setMempool(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  
  const addToBlock = (key) => {
    console.log(key);
    Axios("http://localhost:3001/addToBlock", {
        method: "post",
        data : {
            uid: localStorage.getItem("uid"),
            key: key
        }
    })
        .then((response) => {
        })
        .catch((err) => {
            console.log(err);
        });
}

  useEffect(() => {
    handleForm()
  }, []);

  return (
    <div className="users">
      <Table responsive striped bordered hover>
        <thead>
          <tr style={{ width: 1000 }}>
            <th>Tx Nr</th>
            <th>AMOUNT</th>
            <th>FEE</th>
            <th>FROM</th>
            <th>TO</th>
            <th>SIGNATURE</th>
            <th>INSERT TRANSACTION INTO NEW BLOCK TO BE MINED</th>
          </tr>
        </thead>
        <tbody>
          {mempool != null && Object.keys(mempool).map(function (key) {
            console.log(key)
            return (
              <tr>
                <td>{mempool[key].tx}</td>
                <td>{mempool[key].amount}</td>
                <td>{mempool[key].fee}</td>
                <td>{mempool[key].from}</td>
                <td>{mempool[key].to}</td>
                <td>{mempool[key].signature}</td>
                <td>
                    <Button style={{margin: "3px"}} onClick={() => addToBlock(key)}>Add this transaction</Button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>
        <Button style={{ margin: "5px" }} onClick={handleForm}>Click to reload table</Button>
    </div>
  );
}

export default Mempool;