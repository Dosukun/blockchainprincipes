import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { login , saveuid} from "../Utils/utils";
import { Alert } from "react-bootstrap";
import Axios from "axios";
import "./Components.css";

function LoginForm() {
  let history = useHistory();

  const [error, setError] = useState(0);
  const [email, setEmail] = useState("Email");
  const [Password, setPass] = useState("Password");

  const redir = (data) => {
    console.log(data);
    return history.push({
      pathname: "/" + data,
    });
  };

  const handleForm = () => {
    Axios("http://localhost:3001/login", {
      method: "POST",
      data: {
        email: email,
        password: Password,
      },
    })
      .then((response) => {
        setError(1);
        login(response.data.token);
        saveuid(response.data.uid);
        console.log(response);
        redir("dashboard");
      })
      .catch((err) => {
        setError(2);
        console.log(err);
      });
  };

  return (
    <div className="LoginForm">
      <p>Veuillez entrer vos identifiants!</p>
      <div className="FormBlock">
        <label>
          Email:
          <input
            onChange={(e) => setEmail(e.target.value)}
            className="InputForm"
            type="email"
            placeholder={email}
            name="Email"
          />
        </label>
        <label>
          Password:
          <input
            onChange={(e) => setPass(e.target.value)}
            className="InputForm"
            type="text"
            placeholder={Password}
            name="PassWord"
          />
        </label>
      </div>
      {error === 2 && <Alert variant="danger">Wrong email or password!</Alert>}
      <button className="LoginButton" onClick={() => handleForm()}>
        Login
      </button>
    </div>
  );
}

export default LoginForm;