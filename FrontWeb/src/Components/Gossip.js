import React, { useState, useEffect } from "react";
import { Card, Button, Table } from "react-bootstrap";
import "./Components.css";
import Axios from "axios";
import { login, saveuid } from "../Utils/utils";


function Gossip(props) {
    const [gossip, setGossip] = useState({});

    const guessBalanceStateColor = (gossip) => {
        if (gossip.balanceTest == "failed") return {text: "KO", outline: "danger"};
        if (gossip.balanceTest == "verified") return {text: "OK", outline: "success"};
        return {text: "Check balance", outline: "primary"};
    }

    const guessSigantureStateColor = (gossip) => {
        if (gossip.signTest == "failed") return {text: "KO", outline: "danger"};
        if (gossip.signTest == "verified") return {text: "OK", outline: "success"};
        return {text: "Check signature", outline: "primary"};
    }

    const isGossipValid = (gossip) => {
        return gossip.balanceTest == "verified" && gossip.signTest == 'verified'
    }

    const handleForm = () => {
        Axios("http://localhost:3001/getGossip", {
            method: "post",
            data : {
                uid: localStorage.getItem("uid")
            }
        })
        .then((response) => {
            setGossip(response.data.gossip);
        })
        .catch((err) => {
            console.log(err);
        });
    };

    const checkBalance = (gossipKey, gossipFrom) => {
        Axios("http://localhost:3001/checkGossipBalance", {
            method: "PUT",
            data : {
                uid: localStorage.getItem("uid"),
                gossipKey,
                gossipFrom            
            }
        })
        .then((response) => {
            console.log("yes")
        })
        .catch((err) => {
            console.log(err);
        });
    }

    const checkSignature = (gossipKey, gossipFrom) => {
        Axios("http://localhost:3001/checkGossipSignature", {
            method: "PUT",
            data : {
                uid: localStorage.getItem("uid"),
                gossipKey,
                gossipFrom            
            }
        })
        .then((response) => {
            console.log("yes")
        })
        .catch((err) => {
            console.log(err);
        });
    }

    const sendMempool = (key) => {
        Axios("http://localhost:3001/toMempool", {
            method: "post",
            data : {
                uid: localStorage.getItem("uid"),
                key: key
            }
        })
        .then((response) => {
            handleForm()
        })
        .catch((err) => {
            console.log(err);
        });
    }
    useEffect(() => {
        handleForm()
    }, []);


    return (
        <div>
            <Table responsive striped bordered hover>
                <thead>
                    <tr>
                        <th>Tx Nr</th>
                        <th>AMOUNT</th>
                        <th>FEE</th>
                        <th>FROM</th>
                        <th>TO</th>
                        <th>SIGNATURE</th>
                        <th>BALANCE OK?</th>
                        <th>SIGNATURE OK?</th>
                        <th>SEND TO MEMPOOL</th>
                    </tr>
                </thead>
                <tbody>
                    {gossip != null && Object.keys(gossip).map(function (key) {
                        console.log(key)
                        const balanceButtonState = guessBalanceStateColor(gossip[key])
                        const signatureButtonState = guessSigantureStateColor(gossip[key])
                        return (
                            <tr>
                                <td>{gossip[key].tx}</td>
                                <td>{gossip[key].amount}</td>
                                <td>{gossip[key].fee}</td>
                                <td>{gossip[key].from}</td>
                                <td>{gossip[key].to}</td>
                                <td>{gossip[key].signature}</td>
                                <td><Button variant={balanceButtonState.outline} onClick={() => checkBalance(key, gossip[key].from)}>{balanceButtonState.text}</Button></td>
                                <td><Button variant={signatureButtonState.outline} onClick={() => checkSignature(key, gossip[key].from)}>{signatureButtonState.text}</Button></td>
                                <td><Button disabled={!isGossipValid(gossip[key])} onClick={() => sendMempool(key)}>Send to mempool</Button></td>
                            </tr>
                        ) 
                    })}
                </tbody>
            </Table>
            <Button onClick={handleForm}>Click to reload table</Button>
        </div>
    );
}

export default Gossip;