import React, { useState, useEffect } from "react";
import { Card, Button, Table, Form } from "react-bootstrap";
import "./Components.css";
import axios from "axios";
import SHA256 from "crypto-js/sha256";


function Miner() {
  const [blockNumber, setBlockNumber] = useState(0);
  const [myBlockNumber, setMyBlockNumber] = useState(0);

  const getMyBlockNumber = async () => {
    try {
      axios("http://localhost:3001/getNodeLen", {
        method: "POST",
        data: {
          uid: localStorage.getItem("uid"),
        }
      })
        .then((response) => {
          console.log(response.data.nb);
          setMyBlockNumber(response.data.nb);
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      console.log(e)
    }
  }


  const getNextBlockNumber = async () => {
    try {
      axios("http://localhost:3001/getBlockNumber", {
        method: "POST",
        data: {
          uid: localStorage.getItem("uid"),
        }
      })
        .then((response) => {
          setBlockNumber( response.data.nb)
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      console.log(e)
    }
  }

  const [transactions, setTransactions] = useState({});
  const getTransactions = async () => {
    try {
      axios("http://localhost:3001/getBlockTransaction", {
        method: "POST",
        data: {
          uid: localStorage.getItem("uid"),
        }
      })
        .then((response) => {
          setTransactions(response.data)
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      console.log(e)
    }
  }

  const [prevHash, setPrevHash] = useState(undefined);
  const getPrevHash = async () => {
    try {
      axios("http://localhost:3001/getPrevHash", {
        method: "POST",
        data: {
          uid: localStorage.getItem("uid"),
        }
      })
        .then((response) => {
          setHash("To be mined!")
          setPrevHash(response.data)
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      console.log(e)
    }
  }


  const [hash, setHash] = useState("No previous hash added!");
  const [nonce, setNonce] = useState(0);
  const [load, setLoad] = useState(true);
  const [dif, setDif] = useState(0);
  const getHash = async () => {
    setLoad(false);
    if (prevHash === undefined) {
      setHash("No previous hash added!")
      console.log("test");
      setLoad(true);
    } else {
      setLoad(false);
      console.log(dif);
      try {
        proofOfWork().then(() => {
          setLoad(true);
        });
      } catch (e) {
        console.log(e)
      }
    }
  }

  const proofOfWork = async (hashing) => {
    try {
      let tmp = 0;
      var hashing = SHA256(tmp + blockNumber + prevHash + JSON.stringify(transactions)).toString();
      while (hashing.substring(0, parseInt(dif)) !== Array(parseInt(dif) + 1).join("0")) {
        tmp++;
        hashing = SHA256(tmp + blockNumber + prevHash + JSON.stringify(transactions)).toString();
      }
      setNonce(tmp);
      setHash(hashing);
    } catch (e) {
      console.log(e);
    }
  }


  const sendToNetwork = async () => {
    try {
      axios("http://localhost:3001/addBlockOnNetwork", {
        method: "POST",
        data: {
          nb: blockNumber,
          data: JSON.stringify(transactions),
          prev: prevHash,
          nonce: nonce,
          hash: hash,
          dif: dif,
          uid: localStorage.getItem("uid"),
        },
      })
        .then((response) => {
          console.log(response);
          setHash(response.data);

        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      console.log(e);
    }
  }



  useEffect(() => {
    getMyBlockNumber().then(r =>
        getNextBlockNumber());

    getTransactions().then(r => setTransactions());
  }, []);

 
  return (
    <div className="Partdiv">
      <Card>
        <Card.Header style={{ background: "#79abd9" }}>
          <div className="Sypo">My local Miner Software</div>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            <div className="PresDiv">
              <div className="LeftPresDiv">
              <Button style={{ margin: "5px" }} onClick={() => window.location.reload({forceReload: false})}>Reload table</Button>
              </div>
              <div div className="RightPresDiv" style={{ width: 1500 }}>
                {blockNumber === myBlockNumber &&
                  <Form>
                    <Form.Group>
                      <Form.Label>Block</Form.Label>
                      <Form.Control type="BlockNb" placeholder={blockNumber} readOnly />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>
                        Difficulty
                    </Form.Label>
                      <Form.Control type="hash" placeholder={dif} onChange={(e) => setDif(e.target.value)} />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>
                        Nonce
                    </Form.Label>
                      <Form.Control type="hash" placeholder={nonce} readOnly />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Data</Form.Label>
                      <Table responsive striped bordered hover>
                        <thead>
                          <tr>
                            <th>Tx Nr</th>
                            <th>AMOUNT</th>
                            <th>FEE</th>
                            <th>FROM</th>
                            <th>TO</th>
                            <th>SIGNATURE</th>
                          </tr>
                        </thead>
                        <tbody>
                          {transactions != null && Object.keys(transactions).map(function (key) {
                            return (
                              <tr>
                                <td>{transactions[key].tx}</td>
                                <td>{transactions[key].amount}</td>
                                <td>{transactions[key].fee}</td>
                                <td>{transactions[key].from}</td>
                                <td>{transactions[key].to}</td>
                                <td>{transactions[key].signature}</td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </Table>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Previous hash</Form.Label>
                      <Form.Control type="prev" placeholder={prevHash} readOnly />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Hash</Form.Label>
                      <Form.Control type="hash" placeholder={hash} readOnly />
                    </Form.Group>
                    <Form.Group>
                      {load === true && <Button onClick={() => getHash()} style={{ margin: "5px", width: "250px" }}>Mine</Button>}
                      {load === false && <h1>mining</h1>}
                      <Button onClick={() => getPrevHash()} style={{ margin: "5px", width: "  250px" }}>Insert previous hash</Button><br />
                      <Button onClick={() => sendToNetwork()} style={{ margin: "5px", width: "250px" }}>Send to the network</Button>
                    </Form.Group>
                  </Form>
                }
                {blockNumber !== myBlockNumber && myBlockNumber !== -1 &&
                <div><h1>Some blocks need to be verified!</h1>
                <Button onClick={getMyBlockNumber}>reset</Button></div>}
                {myBlockNumber === -1 &&
                <div><h1>Connect to the network before mining</h1></div>}
              </div>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Miner;
