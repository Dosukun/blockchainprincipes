import React, { useEffect, useState } from "react";
import { Card, Button, Table } from "react-bootstrap";
import "./Components.css";
import axios from "axios";

function NetworkNode(props) {
    const [joined, setJoined] = useState(0);
    const [network, setNodeNetwork] = useState(undefined);

    const handleForm = async () => {
        try {
            const response = await axios.post("http://localhost:3001/addNode", {uid: localStorage.getItem("uid")})
            console.log(response.data)
            getNodes()
        } catch (e) {
            getNodes()
        }
    }
    /*const handleForm = () => {
        axios("http://localhost:3001/addNode", {
            method: "POST",
            data: {
                uid: localStorage.getItem("uid")
            }
        })
            .then((response) => {
                console.log(response.data);
                getNodes();
            })
            .catch((err) => {
                console.log("lol");
                getNodes();
                console.log(err);
            });
    };*/

    const getNodes = async () => {
        try {
            const response = await axios.post("http://localhost:3001/getNode")
            setNodeNetwork(response.data)
        } catch (e) {
            console.log(e)
        }
    }

    const getLongestBlockchain = async () => {
        try {
            const response = await axios.post("http://localhost:3001/getLongestBlockchain", {uid: localStorage.getItem("uid")})
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        getNodes();
    }, []);

    return (
        <body>
            {/*TODO Reset join*/}
            {joined === 0 && <div>
                <Button onClick={() => handleForm()}>Join the network</Button>
                {/*TODO Join the network*/}
            </div>}

            <div>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr style={{ width: 1000 }}>
                            <th>Node Nr</th>
                            <th>Blockchain length</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        (!!network && !!network.length) && network.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <th>
                                        Nr {index}
                                    </th>
                                    <th>
                                        {item.len}
                                    </th>
                                </tr>
                        )
                    })
                    }
                    </tbody>
                </Table>
                <Button style={{ margin: "5px" }} onClick={getNodes}>Click to reload table</Button>
            </div>
            <div>
                <Button style={{ margin: "5px" }} onClick={() => {getLongestBlockchain()}}>Get the longest Blockchain</Button>
            </div>
        </body>
    )
}

export default NetworkNode;