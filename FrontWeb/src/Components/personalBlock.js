import React, {useState, useEffect} from "react";
import {Card, Button, Table, Form, InputGroup} from "react-bootstrap";
import axios from "axios";
import "./Components.css";
import SHA256 from "crypto-js/sha256";


function PersonalBlock(props) {
    const [blockToVerify, setBlock] = useState({});
    const [isBalanceValid, setIsBlockValid] = useState(false);
    const [isHashValid, setIsHashValid] = useState(false);
    const getBlock = async () => {
        try {
            var test = await axios("http://localhost:3001/getBlockToValidate", {
                method: "POST",
                data: {
                    uid: localStorage.getItem("uid"),
                }
            });
            setBlock(test.data);
        } catch (e) {
            console.log(e)
        }
    }

    const [localBlockchain, setBlockchain] = useState({});
    const getLocalBlockchain = async () => {
        try {
            var test = await axios("http://localhost:3001/getLocalBlockchain", {
                method: "POST",
                data: {
                    uid: localStorage.getItem("uid")
                }
            })
            console.log("get de localBlockchain")
            console.log(test.data)
            setBlockchain(test.data);
        } catch (e) {
            console.log(e)
        }
    }

    const verify = async () => {
        var test = Object.values(blockToVerify)[0];
        console.log(test.data);
        var data = {
            nb : test.nb,
            data : test.data,
            prev : test.prev
        };
        var hash = SHA256(test.nonce + test.nb + test.prev + test.data).toString();
        if (hash === test.hash) {
            console.log(true);
            setIsHashValid(true);
            return true
        } else {
            console.log("error : invalid hash");
            return (false);
        }
    };

    const addToBlockchain = async () => {
        var test = Object.values(blockToVerify)[0];
        console.log(test);
        try {
            await axios("http://localhost:3001/addToBlockchain", {
                method: "POST",
                data: {
                    nb: test.nb,
                    hash: test.hash,
                    prev: test.prev,
                    data: test.data,
                    nonce: test.nonce,
                    uid: localStorage.getItem("uid")
                }
            })
        } catch (e) {
            console.log(e)
        }
    };

    const checkBlockBalance = async (transactions) => {
        try {
            await axios("http://localhost:3001/checkBlockBalance", {
                method: "POST",
                data: {
                    transactions
                }
            })
        } catch (error) {
            console.error({error})
        }
        setIsBlockValid(true);
    }

    const updateUsersBalance = async (transactions) => {
        try {
            await axios("http://localhost:3001/updateUsersBalance", {
                method: "PUT",
                data: {
                    transactions,
                    uid: localStorage.getItem("uid")
                }
            })
        } catch (error) {
            console.error({error})
        }
    }

    const refresh = async => {
        getLocalBlockchain();
        getBlock();
    }

    useEffect(() => {
        getBlock();
        getLocalBlockchain();
    }, []);
    return (
        <body>
             <Button onClick={refresh}>Reload</Button>
        <h4>Block waiting for verification</h4>
        <div className="ContainerBlock">
            {blockToVerify !== "none" && Object.keys(blockToVerify).map(function (key) {
                var test = blockToVerify[key].data;
                try {
                    var parsed = JSON.parse(test);
                } catch (e) {
                }
                return (
                    <div className="Block">
                        <div className="AllInfo">
                            <div className="BlockInfo">
                                <Table className="BlockTable" responsive striped bordered hover >
                                    <thead>
                                    <tr>
                                        <th>Number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            {blockToVerify[key].nb}
                                        </td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </div>
                            <div className="BlockInfo">
                                <Table className="BlockTable" responsive striped bordered hover >
                                    <thead>
                                    <tr>
                                        <th>Nonce</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            {blockToVerify[key].nonce}
                                        </td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                        <InputGroup style = {{marginLeft: "5px", marginRight: "5px", marginBottom: "5px"}}>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Hash</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control type="Text" disabled value={blockToVerify[key].hash}/>
                        </InputGroup>
                        <InputGroup style = {{marginLeft: "5px", marginRight: "5px", marginBottom: "5px"}}>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Previous hash</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control type="Text" disabled value={blockToVerify[key].prev}/>
                        </InputGroup>

                        <Table responsive striped bordered hover>
                            <thead>
                            <tr>
                                <th>Tx Nr</th>
                                <th>AMOUNT</th>
                                <th>FEE</th>
                                <th>FROM</th>
                                <th>TO</th>
                                <th>SIGNATURE</th>
                            </tr>
                            </thead>
                            {parsed != null && Object.keys(parsed).map(function (key2) {
                                return (
                                    <tbody>
                                    <tr>
                                        <td>{parsed[key2].tx}</td>
                                        <td>{parsed[key2].amount}</td>
                                        <td>{parsed[key2].fee}</td>
                                        <td>{parsed[key2].from}</td>
                                        <td>{parsed[key2].to}</td>
                                        <td>{parsed[key2].signature}</td>
                                    </tr>
                                    </tbody>
                                )
                            })}
                        </Table>
                        <Button onClick={() => {verify();checkBlockBalance(parsed)}} style={{ margin: "5px", width: "45%" }}>Verify block</Button><br />
                        <Button disabled={!(isBalanceValid && isHashValid)} onClick={() => {addToBlockchain();updateUsersBalance(parsed)}} style={{ margin: "5px", width: "45%" }}>Add to blockchain</Button>
                    </div>
            )
            })}
        </div>
        <h4>My blockchain</h4>
        <div className="ContainerBlock">
            {localBlockchain !== "none" && Object.keys(localBlockchain).map(function (key) {
                try {
                    var test = localBlockchain[key].data;
                    var parsedValues = JSON.parse(test);
                } catch (e) {

                }
                return (
            <div className="Block">
                <div className="AllInfo">
                    <div className="BlockInfo">
                        <Table className="BlockTable" responsive striped bordered hover >
                            <thead>
                            <tr>
                                <th>Number</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {localBlockchain[key].nb}
                                </td>
                            </tr>
                            </tbody>
                        </Table>
                    </div>
                    <div className="BlockInfo">
                        <Table className="BlockTable" responsive striped bordered hover >
                            <thead>
                            <tr>
                                <th>Nonce</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {localBlockchain[key].nonce}
                                </td>
                            </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>
                <InputGroup style = {{marginLeft: "5px", marginRight: "5px", marginBottom: "5px"}}>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Hash</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control type="Text" disabled value={localBlockchain[key].hash}/>
                </InputGroup>
                <InputGroup style = {{marginLeft: "5px", marginRight: "5px", marginBottom: "5px"}}>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Previous hash</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control type="Text" disabled value={localBlockchain[key].prev}/>
                </InputGroup>

                <Table responsive striped bordered hover>
                    <thead>
                    <tr>
                        <th>Tx Nr</th>
                        <th>AMOUNT</th>
                        <th>FEE</th>
                        <th>FROM</th>
                        <th>TO</th>
                        <th>SIGNATURE</th>
                    </tr>
                    </thead>
                    {parsedValues != null && Object.keys(parsedValues).map(function (key2) {
                        return (
                            <tbody>
                            <tr>
                                <td>{parsedValues[key2].tx}</td>
                                <td>{parsedValues[key2].amount}</td>
                                <td>{parsedValues[key2].fee}</td>
                                <td>{parsedValues[key2].from}</td>
                                <td>{parsedValues[key2].to}</td>
                                <td>{parsedValues[key2].signature}</td>
                            </tr>
                            </tbody>
                        )
                    })}
                </Table>
            </div>)
            })}
        </div>
        </body>
    );
}

export default PersonalBlock;

/*
*import React, { useState, useEffect } from "react";
import { Card, Button, Table, Form } from "react-bootstrap";
import axios from "axios";
import "./Components.css";

function PersonalBlock(props) {
    const [myBlockNumber, setMyBlockNumber] = useState(0);
    const getMyBlockNumber = async () => {
        try {
            axios("http://localhost:3001/getNodeLen", {
                method: "POST",
                data: {
                    uid: localStorage.getItem("uid"),
                }
            })
                .then((response) => {
                    console.log("testt"+response.data.nb);
                    setMyBlockNumber(response.data.nb);
                })
                .catch((err) => {
                    console.log(err);
                });
        } catch (e) {
            console.log(e)
        }
    }

    const [blockchain, setBlockchain] = useState([]);
    const getLocalBlockchain = async () => {
        try {
            axios("http://localhost:3001/getLocalBlockchain", {
                method: "GET",
                data: {
                    uid: localStorage.getItem("uid")
                }
            })
                .then((response) => {
                    setBlockchain(response.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        } catch (e) {
            console.log(e)
        }
    }

    const [blockToVerify, setBlock] = useState("undefined");
    const getBlock = async () => {
        try {
            axios("http://localhost:3001/getBlockToValidate", {
                method: "POST",
                data: {
                    nb: myBlockNumber
                }
            })
                .then((response) => {
                    console.log(myBlockNumber);
                    console.log(response.data);
                    setBlock(response.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        getMyBlockNumber();
        getBlock();
    }, []);
    return (
        <body>
            {getLocalBlockchain}
            <Card style={{ width: '90rem' }}>
                <Card.Title>Block to verify</Card.Title>
                <Card.Body>
                    <Form>
                        <Form.Group>
                            <Form.Label>Block nb</Form.Label>
                            <Form.Control type="BlockNb" placeholder={myBlockNumber} readOnly />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Nonce</Form.Label>
                            <Form.Control type="Nonce" placeholder={blockToVerify.nonce} readOnly />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Hash</Form.Label>
                            <Form.Control type="Hash" placeholder={blockToVerify.hash} readOnly />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Previous hash</Form.Label>
                            <Form.Control type="PrevHash" placeholder={blockToVerify.prev} readOnly />
                        </Form.Group>
                        <Card.Link href="#">Verify</Card.Link>
                        <Card.Link href="#">Add to blockchain</Card.Link>
                    </Form>
                </Card.Body>
            </Card>
            <Card style={{ width: '90rem' }}>
                <Card.Title>My blockchain</Card.Title>
                <Card.Body>
                    {blockchain != null && Object.keys(blockchain).map(function (key) {
                        return (
                            <Card>
                                <Card.Text>Block nb : {blockchain.number}</Card.Text>
                                <Form>
                                    <Form.Group>
                                        <Form.Label>Block nb</Form.Label>
                                        <Form.Control type="BlockNb" placeholder={blockchain.blockNb} readOnly />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Nonce</Form.Label>
                                        <Form.Control type="Nonce" placeholder={blockchain.nonce} readOnly />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Hash</Form.Label>
                                        <Form.Control type="Hash" placeholder={blockchain.hash} readOnly />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Previous hash</Form.Label>
                                        <Form.Control type="PrevHash" placeholder={blockchain.prev} readOnly />
                                    </Form.Group>
                                </Form>
                            </Card>
                        )
                    })}
                </Card.Body>
            </Card>
        </body>
    );
}

export default PersonalBlock; */