/* eslint-disable react/no-unescaped-entities */
import React from "react";
import { BiCodeBlock } from "react-icons/bi";
import { useHistory } from "react-router-dom";
import "./Components.css";

function NavBar(props) {
  let history = useHistory();

  const redir = (data) => {
    console.log(data);
    return history.push({
      pathname: "/login",
      props: {
        asAccount: data,
      },
    });
  };

  return (
    <div className="NavBar">
      <div className="LeftBar">
        <div className="BrandIcon">
          <BiCodeBlock />
        </div>
        <h className="BrandTypo">BlockChain Simulator</h>
      </div>
      <div className="RightBar">
        <button
          className="SignInButton"
          onClick={() => {
            redir(false);
          }}
        >
          S'inscrire
        </button>
        <button
          className="ConnectButton"
          onClick={() => {
            redir(true);
          }}
        >
          Connexion
        </button>
      </div>
    </div>
  );
}

export default NavBar;
