import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import { config } from 'dotenv';
import cors from 'cors';
import firebase from 'firebase/app';
import admin from 'firebase-admin';
import apiRouter from './src/index';
import serviceAccount from './firebase.json';
import 'regenerator-runtime/runtime';

config();
require('firebase/auth');
require('firebase/firestore');

const firebaseConfig = {
  apiKey: "AIzaSyBCwcdojR9Nw4ABHWTLpnO_Nzw-9pmoWJQ",
  authDomain: "blockchain-simulator.firebaseapp.com",
  databaseURL: "https://blockchain-simulator.firebaseio.com",
  projectId: "blockchain-simulator",
  storageBucket: "blockchain-simulator.appspot.com",
  messagingSenderId: "42219150294",
  appId: "1:42219150294:web:a9572dc4f65a5ed90de277",
  measurementId: "G-LKHG7E8P2N"
};

firebase.initializeApp(firebaseConfig);
admin.initializeApp({
  serviceAccount: './firebase.json',
  databaseURL: firebaseConfig.databaseURL,
  credential: admin.credential.cert(serviceAccount),
});


const app = express();

app.listen(3001);


if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
}
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', apiRouter);


module.exports = app;
