import express from 'express';
import UserController from './routes/user';
import NodeController from './routes/nodes';
import Market from "./routes/market"
import Miner from "./routes/miner"
const router = express();


//USER
router.post('/register', UserController.register);
router.post('/login', UserController.login);
router.post('/get', UserController.get);
router.put('/regenkey', UserController.regenerateKeys);
router.post('/signKey', UserController.signKey);
//NODES
router.post('/getGossip', NodeController.getGossip);
router.post('/addGossip', NodeController.addGossip);
router.put('/checkGossipBalance', NodeController.checkGossipBalance);
router.put('/checkGossipSignature', NodeController.checkGossipSignature);
router.post('/addNode', NodeController.addNode);
router.post('/getNode', NodeController.getNode);
router.post('/toMempool', NodeController.toMempool);
router.post('/getMempool', NodeController.getMempool);
router.post('/getBlockToValidate', NodeController.getBlockToValidate);
router.post('/getLocalBlockchain', NodeController.getLocalBlockchain);
router.post('/addToBlockchain', NodeController.addToBlockchain);
router.post('/getLongestBlockchain', NodeController.getLongestBlockchain);
router.put('/updateUsersBalance', NodeController.updateUsersBalance);
router.post('/checkBlockBalance', NodeController.checkBlockBalance);
//MARKET
router.post('/addItems', Market.addItems);
router.get('/getItems', Market.getItems);
//MINER
router.post('/addToBlock', Miner.addToBlock);
router.post('/getBlockTransaction', Miner.getBlockTransaction);
router.post('/getPrevHash', Miner.getPrevHash);
router.post('/getBlockNumber', Miner.getBlockNb);
router.post('/addBlockOnNetwork', Miner.addBlockOnNetwork);
router.post('/getNodeLen', Miner.getLen);


module.exports = router;
