import 'firebase/auth';
import admin from 'firebase-admin';
import db_tools from "../tools/db_tools"
import { ref } from 'joi';

const addItems = async (req, res) => {
    var db = admin.database();
    var market = db.ref("/market");
    market.push({
        publicKey: req.body.pub,
        good: req.body.goods,
        amount: req.body.price
    }).then((e) => {
        console.log("OK");
        return res.status(200).send("ok");  
    }).catch(()=> {
        return res.status(400).send("nope");       
    })
  };

  const getItems = async (req, res) => {
    var items = await db_tools.getItems();
    console.log(items);
    return res.status(200).json(items);
  };
  export default {
    addItems, getItems
  };