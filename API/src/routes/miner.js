import firebase from 'firebase/app';
import 'firebase/auth';
import admin from 'firebase-admin';
import db_tools from "../tools/db_tools"
import crypto from "crypto";

const addToBlock = async (req, res) => {
    var node = await db_tools.getNodeByUid(req.body.uid);
    var db = admin.database();
    var ref = db.ref("nodes").child(node).child("mempool").child(req.body.key).once("value");
    var transac = (await ref).val();
    db.ref("nodes").child(node).child("mempool").child(req.body.key).remove();
    db.ref("nodes").child(node).child("block").push({
        tx: transac.tx,
        amount: transac.amount,
        fee: transac.fee,
        from: transac.from,
        signature: transac.signature,
        to: transac.to
    });
};

const getBlockNb = async (req, res) => {
    var node = await db_tools.getNodeByUid(req.body.uid);
    var db = admin.database();
    var ref = db.ref("NB").child("blNb").once("value");
    var nb = (await ref).val();
    return res.send({ nb: nb });
};

const getLen = async (req, res) => {
    var node = await db_tools.getNodeByUid(req.body.uid);
    var db = admin.database();
    var len = db.ref("nodes").child(node).child('len').once("value");
    var test = (await len).val();
    console.log("len" + test);
    return res.status(200).send({ nb: test });
};

const getBlockTransaction = async (req, res) => {
    var node = await db_tools.getNodeByUid(req.body.uid);
    console.log(node);
    var db = admin.database();
    var ref = db.ref("nodes").child(node).child("block").once("value");
    var transac = (await ref).val();
    console.log(transac);
    return res.status(200).send(transac);
};

const getPrevHash = async (req, res) => {
    var node = await db_tools.getNodeByUid(req.body.uid);
    var db = admin.database();
    var ref = db.ref("nodes").child(node).child("len").once("value");
    var len = (await ref).val();
    if (len === 0) {
        return res.status(200).send("0000000000000000000000000000000000000000000000000000000000000000");
    } else {
        var ref = db.ref("nodes").child(node).child("blockchain").once("value");
        var blockchain = (await ref).val();
        var test = ""
        for (var block in blockchain) {
            test = blockchain[block].hash;
        }
        console.log(test);
        return res.status(200).send(test);

    }
};

const addBlockOnNetwork = async (req, res) => {
    console.log(req.body);
    if (req.body.data != '') {
        var node = await db_tools.getNodeByUid(req.body.uid);
        var prev = req.body.prev;
        var data = req.body.data;
        var nonce = req.body.nonce;
        var hash = req.body.hash;
        var db = admin.database();
        var nb = req.body.nb;
        var test = db.ref("NB").child("blNb").once("value");
        var numb = (await test).val();
        if (nb === numb) {
            db.ref("NB").update({blNb: parseInt(numb + 1)});
            var ref = db.ref("/toValidate").child(nb);
            ref.push({
                nb: nb,
                data: data,
                prev: prev,
                hash: hash,
                nonce: nonce
            }).then(() => {
                var ref = db.ref("nodes").child(node).child("block").remove();
                return res.status(200).send("you won the race!");
            }).catch(() => {
                return res.status(200).send("ko");
            })
        } else {
            res.status(200).send("you lose the race, verify new block before mining");
        }
    } else {
        res.status(200).send("no data");
    }
};

export default {
    addToBlock, getBlockTransaction, getPrevHash, getBlockNb, addBlockOnNetwork, getLen
};