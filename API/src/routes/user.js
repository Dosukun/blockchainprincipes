import firebase from 'firebase/app';
import 'firebase/auth';
import admin from 'firebase-admin';
import db_tools from "../tools/db_tools"
import { signData } from '../tools/wallet_tools'
import { checkUserBalanceForTransaction } from '../tools/gossip_tools';

const register = async (req, res) => {
    const payload = req.body;
    try {
        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then((user) => {
                db_tools.createUser(user.user.uid, payload);
                admin
                    .auth()
                    .createCustomToken(user.user.uid)
                    .then((customToken) => {
                        var data = {
                            uid: user.user.uid,
                            token: customToken
                        };
                        return res.status(200).send(data);
                    })
                    .catch((error) => {
                        return res.status(500).send(data);
                        console.log('Error creating custom token:', error);
                    });

            })
            .catch((error) => {
                console.log(error);
            });

    } catch (err) {
        return res.status(500).send(err.data);
    }
};

const login = async (req, res) => {
    try {
        firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
            .then((user) => {
                admin
                    .auth()
                    .createCustomToken(user.user.uid)
                    .then((customToken) => {
                        var data = {
                            uid: user.user.uid,
                            token: customToken
                        };
                        console.log("uid after login", data);
                        return res.status(200).send(data);
                    })
                    .catch((error) => {
                        console.log('Error creating custom token:', error);
                        return res.status(500).send(error);
                    });

            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                return res.status(500).send(errorMessage);
            });
    }
    catch (err) {
        return res.status(500).send(err);
    }
};

const regenerateKeys = async (req, res) => {
    try {
    const uid = req.body.uid;
    const keys = await db_tools.regenerateUserKeyPair(uid);
    res.status(200).send(keys);
    } catch(error) {
        console.error("error", error);
        res.status(400).send({error})
    }
};

const signKey = async (req, res) => {
    try {
        const {data, privateKey} = req.body;
        console.log('data to sign : ', data, 'private key for sign : ', privateKey);
        const signed = await signData(privateKey, data);
        console.log('signed data : ', signed);
        res.status(200).send({signature: signed.toString('hex')});
    } catch(error) {
        console.error("error", error);
        res.status(400).send({error})
    }
};

const get = async (req, res) => {
    var db = admin.database();
    const ref = db.ref("users").child(req.body.uid);
    return await ref.once('value', (user) => res.status(200).send(user.val()))
      .catch((e) => {
        console.log(e);
        return res.status(400).send(e);
      });
};


export default {
    register, login, get, regenerateKeys, signKey
}