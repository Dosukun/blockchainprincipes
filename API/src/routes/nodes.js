import firebase from 'firebase/app';
import 'firebase/auth';
import admin from 'firebase-admin';
import db_tools from "../tools/db_tools"
import {checkUserBalanceForTransaction, checkSignatureForTransaction} from "../tools/gossip_tools"
import {updateBalanceFromTransaction, verifyBill} from "../tools/node_tools"
import {parse} from "dotenv";

const addGossip = async (req, res) => {
  var db = admin.database();
  const ref = db.ref("gossip");
  var gossip = await db.ref("/NB").child("txNb").once("value", (snap) => {
    return snap.val();
  }).then((tx) => {
    var gossip = {
      tx: tx.val(),
      amount: req.body.amount,
      fee: req.body.fee,
      from: req.body.from,
      signature: req.body.signature,
      to: req.body.to,
      balanceTest: "toVerify",
      signTest: "toVerify",
    }
    return gossip;
  });
  var allNode = db.ref("/nodes");
  allNode.once("value", (snap) => {
    for (var node in snap.val()) {
      db.ref("/nodes").child(node).child("gossip").push(
        gossip
      )
    }
  });
  db_tools.incrementTxNb();
  return res.status(200).send("ok");
};

const getGossip = async (req, res) => {
  var userGossip = await db_tools.getGossipByUid(req.body.uid);
  return res.status(200).json(userGossip);
};

const addNode = async (req, res) => {
  var uid = req.body.uid;
  var db = admin.database();
  var user = await db.ref("/users").child(uid).once("value");
  var nb = await db.ref("/NB").child("ndNb").once("value");
  if (user.val().nodes === false) {
    db.ref("/users").child(uid).update(
      {
        nodes: true
      }
    )
    db.ref("nodes").child(nb.val()).set({
      uid: uid,
      len: 0
    })
    db_tools.incrementNodeNb();
    return res.status(200).send(nb);
  } else return res.status(400).send("nope");

};

const getNode = async (req, res) => {
  var nodes = await db_tools.getNodes();
  return res.status(200).json(nodes);
};

const toMempool = async (req, res) => {
  var node = await db_tools.getNodeByUid(req.body.uid);
  var db = admin.database();
  var ref = db.ref("nodes").child(node).child("gossip").child(req.body.key).once("value");
  var gossip = (await ref).val();
  db.ref("nodes").child(node).child("gossip").child(req.body.key).remove();
  db.ref("nodes").child(node).child("mempool").push({
    tx: gossip.tx,
    amount: gossip.amount,
    fee: gossip.fee,
    from: gossip.from,
    signature: gossip.signature,
    to: gossip.to,
  });
};

const getMempool = async (req, res) => {
  var node = await db_tools.getNodeByUid(req.body.uid);
  var db = admin.database();
  var ref = db.ref("nodes").child(node).child("mempool").once("value");
  return res.status(200).json((await ref).val());
};

const getBlockToValidate = async (req, res) => {
  var node = await db_tools.getNodeByUid(req.body.uid);
  var db = admin.database();
  var len = db.ref("nodes").child(node).child("len").once("value");
  var len2 = (await len).val();
  console.log("test"+ len2);
  var ref = db.ref("/toValidate").child(len2).once("value");
  var inst = (await ref).val();
  console.log("YOOOOOOOOOOOOOOOOOOO3"+inst);
  if (inst === null) {
    return res.status(200).send("none");
  } else {
    return res.status(200).send(inst);
  }
};

const getLocalBlockchain = async (req, res) => {
  var db = admin.database();
  var node = await db_tools.getNodeByUid(req.body.uid);
  var ref = db.ref("nodes").child(node).child("blockchain").once("value");
  var inst = (await ref).val();
  console.log(inst);
  if (inst === null) {
    return res.status(200).send("none");
  } else {
    return res.status(200).send(inst);
  }
};

function ModifyGossipAndMempool(data) {
    console.log("ModifyGossipAndMempool called")
    var parsed = JSON.parse(data);
    var nodes = db_tools.getNodes()

  Object.keys(parsed).forEach( async function(key) {
    nodes.then(function(result) {
      Object.keys(result).forEach(function(resultKey) {
        if (result[resultKey].gossip) {
          Object.keys(result[resultKey].gossip).forEach(function(gossipKey) {
            if (result[resultKey].gossip[gossipKey].tx === parsed[key].tx)
            {
              delete result[resultKey].gossip[gossipKey]
            }
          })
        }

        if (result[resultKey].mempool) {
          Object.keys(result[resultKey].mempool).forEach(function(mempoolKey) {
            if (result[resultKey].mempool[mempoolKey].tx === parsed[key].tx)
            {
              delete result[resultKey].mempool[mempoolKey]
            }
          })
        }
      });
      var db = admin.database();
      const ref = db.ref("nodes").set(result);
      /*    result.then(function(result) {
            console.log("\nPrint finale delete key : " + key)
            Object.keys(result).forEach(function(key) {
              console.log("\n=== For node : " + key + " ===")
              Object.keys(result[key].gossip).forEach(function(gossipKey) {
                console.log(gossipKey)

              })

            })
        })*/
    })
})}

const addToBlockchain = async (req, res) => {
  var db = admin.database();
  var node = await db_tools.getNodeByUid(req.body.uid);
  var ref = db.ref("nodes").child(node).child("blockchain").once("value");
  var inst = (await ref).val();
  console.log(inst);
  if (inst === null) {
    db.ref("nodes").child(node).child("blockchain").push({
      nb: req.body.nb,
      hash: req.body.hash,
      prev: req.body.prev,
      data: req.body.data,
      nonce: req.body.nonce,
    })
    console.log("pop" + parseInt(req.body.nb + 1));
    db.ref("nodes").child(node).update({ len: parseInt(req.body.nb + 1) });
    return res.status(200).send("none");
  } else {
    db.ref("nodes").child(node).child("blockchain").push({
      nb: req.body.nb,
      hash: req.body.hash,
      prev: req.body.prev,
      data: req.body.data,
      nonce: req.body.nonce,
    })
    console.log("popi" + parseInt(req.body.nb + 1));
    db.ref("nodes").child(node).update({ len: parseInt(req.body.nb + 1) }).then(
      ModifyGossipAndMempool(req.body.data)
    );
    return res.status(200).send(inst);
  }
};

const getLongestBlockchain = async (req, res) => {
  var db = admin.database();
  var toMod = await db_tools.getNodeByUid(req.body.uid);
  var nodes = await db_tools.getNodes();
  var high = 0;
  var toCop = 0;
  for (var node in nodes) {
    if (high < nodes[node].len) {
      high = nodes[node].len;
      toCop = node;
    }
  }
  var blockchain = db.ref("nodes").child(toCop).child("blockchain").once("value");
  var actual = (await blockchain).val();
  var user = db.ref("nodes").child(toMod).child("blockchain").set(actual);
  var user = db.ref("nodes").child(toMod).child("len").set(high);
  return res.send("ok");
};

const checkGossipBalance = async (req, res) => {
  try {
      const isValid = await checkUserBalanceForTransaction(req.body.uid, req.body.gossipKey, req.body.gossipFrom);
      res.status(200).send({isValid});
  } catch(error) {
      console.error("error", error);
      res.status(400).send({error})
  }
}

const checkGossipSignature = async (req, res) => {
  try {
    const isValid = await checkSignatureForTransaction(req.body.uid, req.body.gossipKey, req.body.gossipFrom);
    res.status(200).send({isValid});
  } catch(error) {
      console.error("error", error);
      res.status(400).send({error})
  }
}

const updateUsersBalance = async (req, res) => {
  try {
    Object.keys(req.body.transactions).forEach((transactionId) => {
      updateBalanceFromTransaction(req.body.transactions[transactionId], req.body.uid);
    });
    res.status(200).send({ok: true});
  } catch(error) {
    console.error("error", error);
    res.status(400).send({error});
  }
}

const checkBlockBalance = async (req, res) => {
  try {
    await verifyBill(req.body.transactions)
    res.status(200).send({ok: true});
  } catch(error) {
    console.error("error", error);
    res.status(400).send({error});
  }
}

export default {
  addGossip, getGossip, addNode, getNode, toMempool, getMempool, getBlockToValidate, addToBlockchain, getLocalBlockchain, getLongestBlockchain, checkGossipBalance, checkGossipSignature, updateUsersBalance, checkBlockBalance
};