import 'firebase/auth';
import admin from 'firebase-admin';
import { generateKeyPair } from "crypto"
import { Console } from 'console';
import { genKeyPair } from './wallet_tools'


const createUser = (uid, payload) => {
    var db = admin.database();
    try {
        const keys = genKeyPair();
        db.ref("/users").child(uid).set({
            nodes : false,
            firstname: payload.firstname,
            lastname: payload.lastname,
            email: payload.email,
            password: payload.password,
            balance: 50000,
            publicKey: keys.publicKey.toString('hex'),
            privateKey: keys.privateKey.toString('hex')
        }).then((de) => {
            console.log("succesfully added");
        }).catch((err) => { console.log(err) });
    } catch (error) {
        console.log("Err is: ", error);
    }
};

const regenerateUserKeyPair = async (uid) => {
    var db = admin.database();
    try {
        const keys = genKeyPair();
        await db.ref("/users").child(uid).update({
            publicKey: keys.publicKey.toString('hex'),
            privateKey: keys.privateKey.toString('hex')
        })
        console.log("key succesfully generated");
        return {publicKey: keys.publicKey.toString('hex'),privateKey: keys.privateKey.toString('hex')};
    } catch (error) {
        console.log("Err is: ", error);
    }
};

const getGossipById = async (uid) => {
    var db = admin.database();
    var ref = db.ref("/gossip").child(uid);
    return await ref.once('value', (gossip) => {
        return gossip
    })
        .catch((e) => {
            console.log(e);
        });
};

const getUserIdByPublicKey = (publicKey) => {
    var db = admin.database();
    var ref = db.ref("/users");
    return ref.once('value').then((user) => {
        var users = user.val();
        for (var uid in users) {
            if (users[uid].publicKey === publicKey) {
                return uid
            }
        }
    })
};


const incrementNodeNb = async () => {
    var db = admin.database();
    db.ref("/NB").child("ndNb").once("value", (snap) => {
        return snap;
    }).then((nb) => {
        db.ref("/NB").update(
            {
                ndNb: parseInt(nb.val(), 10) + 1
            }   
        )
    }
    )
};

const incrementTxNb = async () => {
    var db = admin.database();
    db.ref("/NB").child("txNb").once("value", (snap) => {
        return snap;
    }).then((nb) => {
        db.ref("/NB").update(
            {
                txNb: parseInt(nb.val(), 10) + 1
            }   
        )
    }
    )
};

const resetNodeNb = () => {
    var db = admin.database();
    db.ref("/NB").set({
        ndNb: 0
    });
};

const getGossipByUid = (uid) => {
    var db = admin.database();
    const ref = db.ref("nodes");
    return ref.once('value').then((user) => {
        var test = user.val();
        for (var ent in test) {
            if (test[ent].uid === uid) {
                return (test[ent])
            }
        }
        return ("nope");
    })
};

const getNodeByUid = (uid) => {
    var db = admin.database();
    const ref = db.ref("nodes");
    return ref.once('value').then((user) => {
        var test = user.val();
        for (var ent in test) {
            if (test[ent].uid === uid) {
                return (ent);
            }
        }
        return ("nope");
    })
};

const getNodes = () => {
    var db = admin.database();
    const ref = db.ref("nodes");
    return ref.once('value').then((user) => {
        var test = user.val();
            return (test);
    })
};

const getItems = () => {
    var db = admin.database();
    const ref = db.ref("market");
    return ref.once('value').then((items) => {
        var test = items.val();
            return (test);
    })
};

module.exports = {
    createUser, getGossipById, getUserIdByPublicKey, regenerateUserKeyPair, incrementNodeNb, resetNodeNb, incrementTxNb, getGossipByUid, getNodes, getNodeByUid, getItems
};