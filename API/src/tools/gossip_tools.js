import {verify, createPublicKey} from "crypto";
import db_tools from "./db_tools"
import admin from 'firebase-admin';

// those functions will be my eternal damnation has one of the crapiest code i have ever made
// so i'll try to put comment for the sanity of future reader
// AND YES this could (should) have been factorized in at least 4 (if not more) differente function
// but i'm seriously lacking the time to do it

export const checkUserBalanceForTransaction = async (uid, gossipKey, gossipPublicKey) => {
    // getting gossip from node in bdd
    var db = admin.database();
    var node = await db_tools.getNodeByUid(uid);
    var refGossip = db.ref("nodes").child(node).child("gossip").child(gossipKey);
    var gossip = (await refGossip.once("value")).val();

    try {
        // getting the owner owner of the transaction with his publicKey
        const userIdFromPublicKey = await db_tools.getUserIdByPublicKey(gossipPublicKey);
        const originUserDataFromUid = db.ref("users").child(userIdFromPublicKey);
        const originUser = await originUserDataFromUid.once('value');
        
        // checking the balance of the creator of the transaction
        const isBalanceOk = originUser.val().balance >= gossip.amount;

        // updating the gossip in bdd
        if (isBalanceOk) {
            await refGossip.update({balanceTest: "verified"});
        } else {
            await refGossip.update({balanceTest: "failed"});
        }
        return isBalanceOk;
    } catch(error) {
        console.error("error", error);
        await refGossip.update({balanceTest: "failed"});
    }
}

export const checkSignatureForTransaction = async (uid, gossipId, gossipPublicKey) => {
    // getting gossip from node in bdd
    var db = admin.database();
    var node = await db_tools.getNodeByUid(uid);
    var refGossip = db.ref("nodes").child(node).child("gossip").child(gossipId);
    var gossip = (await refGossip.once("value")).val();

    try {
        // getting the owner owner of the transaction with his publicKey
        const userIdFromPublicKey = await db_tools.getUserIdByPublicKey(gossipPublicKey);
        const originUserDataFromUid = db.ref("users").child(userIdFromPublicKey);
        const originUser = await originUserDataFromUid.once('value');

        // data to verify
        const data={
            amount: gossip.amount,
            fee: gossip.fee,
            from: gossip.from,
            to: gossip.to
        }
        console.log(data);

        // verify process
        const rawPublicKey = Buffer.from(originUser.val().publicKey, 'hex');
        const keyObject = createPublicKey({key: rawPublicKey, format: 'der', type: 'pkcs1'});
        const rawSignature = Buffer.from(gossip.signature, 'hex');
        const isVerifyOk = verify('RSA-SHA256', Buffer.from(JSON.stringify(data)), keyObject, rawSignature)

        // updating the gossip in bdd
        if (isVerifyOk) {
            await refGossip.update({signTest: "verified"});
        } else {
            await refGossip.update({signTest: "failed"});
        }
        return isVerifyOk;
    } catch(error) {
        console.error("error", error);
        await refGossip.update({signTest: "failed"});
    }
}