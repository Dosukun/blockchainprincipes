import { generateKeyPairSync, createSign, createPrivateKey} from "crypto";

export function genKeyPair() {
  const keys = generateKeyPairSync('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: {
      type: 'pkcs1',
      format: 'der'
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'der',
      cipher: 'aes-256-cbc',
      passphrase: 'top secret'
    }
  });
  return keys;
}

export function signData(privatekey, data) {
  return new Promise((resolve, reject) => {
    const rawKey = Buffer.from(privatekey, 'hex');
    const sign = createSign('RSA-SHA256')
    sign.write(data,(error) => {
      if (error) {
        reject(error);
        return;
      }
    });
    sign.end();
    const keyObject = createPrivateKey({key: rawKey, format: 'der', type: 'pkcs8', passphrase: 'top secret'});
    const signed = sign.sign(keyObject);
    resolve(signed);
  });
}