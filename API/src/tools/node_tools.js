import admin from 'firebase-admin';
import db_tools from "./db_tools"

export const updateBalanceFromTransaction = async (transaction, minerId) => {
    var db = admin.database();
    const originUserIdFromPublicKey = await db_tools.getUserIdByPublicKey(transaction.from);
    const originUserDataFromUid = db.ref("users").child(originUserIdFromPublicKey);
    const originUser = await originUserDataFromUid.once('value');
    
    let result = parseFloat(originUser.val().balance) - parseFloat(transaction.amount) - parseFloat(transaction.fee)
    await originUserDataFromUid.update({balance: result})

    const destinationUserIdFromPublicKey = await db_tools.getUserIdByPublicKey(transaction.to);
    const destinationUserDataFromUid = db.ref("users").child(destinationUserIdFromPublicKey);
    const destinationUser = await destinationUserDataFromUid.once('value');
    
    result = parseFloat(destinationUser.val().balance) + parseFloat(transaction.amount)
    await destinationUserDataFromUid.update({balance: result})

    const minerUserDataFromUid = db.ref("users").child(minerId);
    const minerUser = await minerUserDataFromUid.once('value');
    
    result = parseFloat(minerUser.val().balance) + parseFloat(transaction.fee)
    await minerUserDataFromUid.update({balance: result})
}

export const verifyBill = async (transactions) => {
    var db = admin.database();
    const totalByUser = {};
    const userFromPKs = []

    Object.keys(transactions).forEach((tID) => {

        const transaction = transactions[tID];

        let res = totalByUser[transaction.from] === undefined ? 0 : totalByUser[transaction.from];
        res += parseFloat(transaction.amount) + parseFloat(transaction.fee)
        totalByUser[transaction.from] = res;
        
        if (!userFromPKs.includes(transaction.from))
            userFromPKs.push(transaction.from)
    })

    await Promise.all(userFromPKs.map(async (userFromPK) => {

        const userIdFromPublicKey = await db_tools.getUserIdByPublicKey(userFromPK);
        const userDataFromUid = db.ref("users").child(userIdFromPublicKey);
        const userFrom = await userDataFromUid.once('value');

        if (userFrom.val().balance < totalByUser[userFromPK])
            throw Error(`User ${userFrom.val().email} balance is too low`)
    }))    
}